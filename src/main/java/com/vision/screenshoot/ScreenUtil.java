package com.vision.screenshoot;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ScreenUtil {

    public static BufferedImage captureImageFullScreen() {
        Robot robot = null;
        try {
            robot = new Robot();
        } catch (AWTException e) {return null;}

        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        BufferedImage imageinMemory = robot.createScreenCapture(new Rectangle(screenSize));
        return imageinMemory;
    }


    public static BufferedImage captureImageByRegion(final Rectangle rectangle) {
        Robot robot = null;
        try {
            robot = new Robot();
        } catch (AWTException e) {return null;}

        BufferedImage imageinMemory = robot.createScreenCapture(rectangle);
        return imageinMemory;
    }

    public static void saveImage(final BufferedImage image,final String path){
        try {
            ImageIO.write(image, "png", new File(path));
        } catch (IOException e) {}
    }

}
