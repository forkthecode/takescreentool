package com.vision.screenshoot;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;

public final class ScreenShoot extends JFrame implements MouseListener {

    /**
     *
     */
    private static final long serialVersionUID = -5921590048358794117L;
    private BufferedImage imageInMemory;
    private Rectangle captureRect;
    private final String PATH_TO_SAVE_IMAGE =  System.getProperty("user.dir") +"\\"+"screen.png";


    public ScreenShoot() {

        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setUndecorated(true);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);


        addMouseListener(this);
        addMouseMotionListener(new MouseMotionListener() {

            Point start = new Point();


            public void mouseMoved(MouseEvent me) {
                start = me.getPoint();
                repaint();

            }


            public void mouseDragged(MouseEvent me) {
                Point end = me.getPoint();
                captureRect = new Rectangle(start, new Dimension(end.x - start.x, end.y - start.y));
                repaint();

            }
        });


        setLayout(new FlowLayout());
        imageInMemory = ScreenUtil.captureImageFullScreen();
        pack();
    }

    public void start() {
        setAlwaysOnTop(true);
        setVisible(true);
    }

    @Override
    public void paint(Graphics graphics) {
        Graphics2D g = (Graphics2D) graphics;

        g.drawImage(imageInMemory, 0, 0, this);

        if (captureRect != null) {
            g.setColor(Color.RED);
            g.draw(captureRect);
        }

        g.dispose();
    }

    public void mouseClicked(MouseEvent arg0) {
    }

    public void mouseEntered(MouseEvent arg0) {
    }

    public void mouseExited(MouseEvent arg0) {
    }


    public void mousePressed(MouseEvent arg0) {
    }

    public void mouseReleased(MouseEvent arg0) {

        if(captureRect != null){
            BufferedImage imageSelected = ScreenUtil.captureImageByRegion(captureRect);
            ScreenUtil.saveImage(imageSelected,PATH_TO_SAVE_IMAGE);
            dispose(); // Free and close the current window
        }
    }
}
