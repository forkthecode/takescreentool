package com.vision.imageviewer;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ImageViewer extends JFrame {

    /**
     *
     */
    private static final long serialVersionUID = 6088328090836723904L;

    private final String IMAGE_PATH = System.getProperty("user.dir") + "\\" + "screen.png";
    private BufferedImage imageInMemory;
    private JLabel picture = new JLabel();
    private final int HEIGTH = 300;
    private final int WIDTH = 300;

    public ImageViewer() {
        setupWindow();

        loadImageFromDisk();
        if (imageInMemory != null) {
            picture.setIcon(new ImageIcon(imageInMemory));
        }

        addComponents();
    }

    private void addComponents() {
        setLayout(new FlowLayout());
        add(picture);
        pack();
    }

    private void setupWindow() {
        setBounds(0, 0, WIDTH, HEIGTH);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    public void start() {
        setAlwaysOnTop(true);
        setVisible(true);
    }

    private void loadImageFromDisk() {
        try {
            imageInMemory = ImageIO.read(new File(IMAGE_PATH));
        } catch (IOException e) {
        }
    }
}
