package com.vision.application;

import com.vision.imageviewer.ImageViewer;
import com.vision.screenshoot.ScreenShoot;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class MainWindowApplication extends JFrame implements MouseListener {


    /**
     *
     */
    private static final long serialVersionUID = 4975762628170976804L;
    private JButton screenShoot = new JButton("ScreenShoot");
    private JButton imageViewer = new JButton("imageViewer");

    public MainWindowApplication() {
        setupWindow();

        attachEvents();

        addComponents();
    }

    private void addComponents() {
        add(imageViewer);
        add(screenShoot);
        pack();
    }

    private void attachEvents() {
        screenShoot.addMouseListener(this);
        imageViewer.addMouseListener(this);
    }

    private void setupWindow() {
        setTitle("Take a Screen tool");
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setLayout(new FlowLayout());
    }

    public void start() {
        setVisible(true);

    }

    public void mouseClicked(MouseEvent evt) {

        if (evt.getSource() == screenShoot) {
            setState(Frame.ICONIFIED);
            ScreenShoot snpTool = new ScreenShoot();
            snpTool.start();
        }

        if(evt.getSource() == imageViewer){
            ImageViewer imgViewer = new ImageViewer();
            imgViewer.start();
        }

    }

    public void mousePressed(MouseEvent mouseEvent) {

    }

    public void mouseReleased(MouseEvent mouseEvent) {

    }

    public void mouseEntered(MouseEvent mouseEvent) {

    }

    public void mouseExited(MouseEvent mouseEvent) {

    }
}
