package com.vision.application;

import javax.swing.*;

public final class EntryPoint {

    public static void main(String[] args) {

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                MainWindowApplication windowApp = new MainWindowApplication();
                windowApp.start();
            }
        });
    }

}
